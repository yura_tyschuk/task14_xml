<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>


  <xsl:template match="/">
    <html>
      <body style="font-family: TimesNewRoman; font-size: 12pt; background-color: white">
        <div style="background-color: white; color:black;">
          <h2>Computers</h2>
        </div>
        <table border="2 ">
          <tr bgcolor="white">
            <th>computer name</th>
            <th>origin</th>
            <th>price</th>
            <th>critical</th>
            <th>ports</th>
            <th>periphery</th>
            <th>energy consumption</th>
            <th>cooler</th>
            <th>processor</th>
            <th>RAM</th>
            <th>video card</th>
            <th>mother board</th>
          </tr>

          <xsl:for-each select="/computers/computer">
            <xsl:sort select="computerName" order="ascending"/>
            <tr>
              <td>
                <xsl:value-of select="computerName"/>
              </td>
              <td>
                <xsl:value-of select="origin"/>
              </td>
              <td>
                <xsl:value-of select="price"/>
              </td>
              <td>
                <xsl:value-of select="critical"/>
              </td>
              <td>
                <xsl:value-of select="ports"/>
              </td>
              <td>
                <xsl:value-of select="types/periphery"/>
              </td>

              <td>
                <xsl:value-of select="types/energyConsumption"/>
              </td>

              <td>
                <xsl:value-of select="types/cooler"/>
              </td>

              <td>
                <xsl:value-of select="types/componentsGroup/processor"/>
              </td>

              <td>
                <xsl:value-of select="types/componentsGroup/RAM"/>
              </td>

              <td>
                <xsl:value-of select="types/componentsGroup/videoCard"/>
              </td>

              <td>
                <xsl:value-of select="types/componentsGroup/motherBoard"/>
              </td>
            </tr>

          </xsl:for-each>

        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>