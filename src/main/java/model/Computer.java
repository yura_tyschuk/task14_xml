package model;

import java.util.List;

public class Computer implements Comparable<Computer> {

  private int computerNumber;
  private String computerName;
  private String origin;
  private double price;
  private Type type;
  private List<Ports> portsList;
  private boolean critical;

  public Computer() {

  }

  public Computer(int computerNumber, String computerName, String origin, double price, Type type,
      List<Ports> portsList, boolean critical) {
    this.computerNumber = computerNumber;
    this.computerName = computerName;
    this.origin = origin;
    this.price = price;
    this.type = type;
    this.portsList = portsList;
    this.critical = critical;
  }

  public List<Ports> getPortsList() {
    return portsList;
  }

  public void setPortsList(List<Ports> portsList) {
    this.portsList = portsList;
  }

  public int getComputerNumber() {
    return computerNumber;
  }

  public void setComputerNumber(int computerNumber) {
    this.computerNumber = computerNumber;
  }

  public String getComputerName() {
    return computerName;
  }

  public void setComputerName(String computerName) {
    this.computerName = computerName;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
  }

  public boolean isCritical() {
    return critical;
  }

  public void setCritical(boolean critical) {
    this.critical = critical;
  }

  @Override
  public String toString() {
    return "Computer number: " + computerNumber + " computer name: " + computerName
        + " origin: " + origin + " price: " + price + " type: " + type
        + " critical: " + critical + "\n";
  }

  @Override
  public int compareTo(Computer o) {
    return this.computerName.compareTo(o.computerName);
  }
}
