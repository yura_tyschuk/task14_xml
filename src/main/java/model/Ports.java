package model;

public class Ports {

  private String comPort;
  private String usbPort;
  private String lptPort;
  private String textContent;

  public Ports(String textContent) {
    this.textContent = textContent;
  }

  public Ports() {
  }

  public String getComPort() {
    return comPort;
  }

  public void setComPort(String comPort) {
    this.comPort = comPort;
  }

  public String getUsbPort() {
    return usbPort;
  }

  public void setUsbPort(String usbPort) {
    this.usbPort = usbPort;
  }

  public String getLptPort() {
    return lptPort;
  }

  public void setLptPort(String lptPort) {
    this.lptPort = lptPort;
  }

  public void setName(String textContent) {
    this.textContent = textContent;
  }

  @Override
  public String toString() {
    return textContent;
  }

}
