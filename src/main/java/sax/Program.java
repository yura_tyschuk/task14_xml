package sax;

import java.io.File;
import java.util.List;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import model.Computer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Program {

  private static Logger logger = LogManager.getLogger();

  public static void main(String[] args) {
    try {
      String schemaName = "computerXSD.xsd";
      String xmlName = "computerXML.xml";
      SaxValidation saxValidation = new SaxValidation(schemaName, xmlName);
      saxValidation.validateXml();


      SAXParserFactory factory = SAXParserFactory.newInstance();
      SAXParser saxParser = factory.newSAXParser();
      MySaxParser mySaxParser = new MySaxParser();
      List<Computer> computerList;
      File file = new File("computerXML.xml");
      saxParser.parse(file, mySaxParser);
      computerList = mySaxParser.getComputerList();
      logger.warn(computerList);
    } catch (Exception e) {
      e.printStackTrace();
    }

  }
}
