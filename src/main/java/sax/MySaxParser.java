package sax;

import java.util.ArrayList;
import java.util.List;
import model.ComponentsGroup;
import model.Computer;
import model.Ports;
import model.Type;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class MySaxParser extends DefaultHandler {

  private ComponentsGroup componentsGroup;
  private Type type;
  private Computer computer = new Computer();
  private Ports ports;
  private List<Computer> computerList = new ArrayList<>();
  private List<Ports> port;
  private boolean cName;
  private boolean cOrigin;
  private boolean cPrice;
  private boolean cCritical;
  private boolean cPorts;
  private boolean cPort;
  private boolean cTypes;
  private boolean cPeriphery;
  private boolean cEnergyConsumption;
  private boolean cCooler;
  private boolean cComponentsGroup;
  private boolean cProcessor;
  private boolean cRAM;
  private boolean cVideoCard;
  private boolean cMotherBoard;

  public List<Computer> getComputerList() {
    return this.computerList;
  }

  @Override
  public void startElement(String uri, String localName,
      String qName, Attributes attributes) {
    if (qName.equals("computer")) {
      String computerN = attributes.getValue("computerNumber");
      Computer computer = new Computer();
      computer.setComputerNumber(Integer.parseInt(computerN));
    } else if (qName.equals("computerName")) {
      cName = true;
    } else if (qName.equals("origin")) {
      cOrigin = true;
    } else if (qName.equals("price")) {
      cPrice = true;
    } else if (qName.equals("critical")) {
      cCritical = true;
    } else if (qName.equals("ports")) {
      cPorts = true;
    } else if (qName.equals("port")) {
      cPort = true;
    } else if (qName.equals("types")) {
      cTypes = true;
    } else if (qName.equals("periphery")) {
      cPeriphery = true;
    } else if (qName.equals("energyConsumption")) {
      cEnergyConsumption = true;
    } else if (qName.equals("cooler")) {
      cCooler = true;
    } else if (qName.equals("componentsGroup")) {
      cComponentsGroup = true;
    } else if (qName.equals("processor")) {
      cProcessor = true;
    } else if (qName.equals("RAM")) {
      cRAM = true;
    } else if (qName.equals("videoCard")) {
      cVideoCard = true;
    } else if (qName.equals("motherBoard")) {
      cMotherBoard = true;
    }

  }

  @Override
  public void endElement(String uri, String localName, String qName) {
    if (qName.equals("computer")) {
      computerList.add(computer);
    }
  }

  @Override
  public void characters(char[] ch, int start, int length) {
    if (cName) {
      computer.setComputerName(new String(ch, start, length));
      cName = false;
    } else if (cOrigin) {
      computer.setOrigin(new String(ch, start, length));
      cOrigin = false;
    } else if (cPrice) {
      computer.setPrice(Integer.parseInt(new String(ch, start, length)));
      cPrice = false;
    } else if (cCritical) {
      computer.setCritical(Boolean.parseBoolean(new String(ch, start, length)));
      cCritical = false;
    } else if (cPorts) {
      port = new ArrayList<>();
      cPorts = false;
    } else if (cPort) {
      ports = new Ports();
      ports.setName(new String(ch, start, length));
      port.add(ports);
      cPort = false;
    } else if (cTypes) {
      type = new Type();
      cTypes = false;
    } else if (cPeriphery) {
      type.setPeriphery(Boolean.parseBoolean(new String(ch, start, length)));
      cPeriphery = false;
    } else if (cEnergyConsumption) {
      type.setEnergyConsumption(Double.parseDouble(new String(ch, start, length)));
      cEnergyConsumption = false;
    } else if (cCooler) {
      type.setCooler(Boolean.parseBoolean(new String(ch, start, length)));
      cCooler = false;
    } else if (cComponentsGroup) {
      componentsGroup = new ComponentsGroup();
      cComponentsGroup = false;
    } else if (cProcessor) {
      componentsGroup.setProcessor(new String(ch, start, length));
      cProcessor = false;
    } else if (cRAM) {
      componentsGroup.setRAM(Integer.parseInt(new String(ch, start, length)));
      cRAM = false;
    } else if (cVideoCard) {
      componentsGroup.setVideoCard(new String(ch, start, length));
      cVideoCard = false;
    } else if (cMotherBoard) {
      componentsGroup.setMotherBoard(new String(ch, start, length));
      type.setComponentsGroup(componentsGroup);
      computer.setType(type);
      computer.setPortsList(port);
      cMotherBoard = false;
    }
  }
}
