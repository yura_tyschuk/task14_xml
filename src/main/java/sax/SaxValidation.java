package sax;

import java.io.File;
import javax.xml.XMLConstants;
import javax.xml.transform.sax.SAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.InputSource;

public class SaxValidation {

  private String schemaName;
  private String xmlName;
  private Schema schema;
  private static Logger logger = LogManager.getLogger();

  public SaxValidation(String schemaName, String xmlName) {
    this.schemaName = schemaName;
    this.xmlName = xmlName;
    schema = loadSchema(schemaName);
  }

  public void validateXml() {
    try {

      Validator validator = schema.newValidator();
      logger.debug("Validator Class: " + validator.getClass().getName());
      SAXSource source = new SAXSource(new InputSource(new java.io.FileInputStream(xmlName)));

      validator.validate(source);
      logger.debug("Validation passed.");

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public Schema loadSchema(String name) {
    Schema schema = null;
    try {
      String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
      SchemaFactory factory = SchemaFactory.newInstance(language);
      schema = factory.newSchema(new File(name));
    } catch (Exception e) {
      e.printStackTrace();
    }
    return schema;
  }

}
