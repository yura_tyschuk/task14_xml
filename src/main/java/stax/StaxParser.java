package stax;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import model.ComponentsGroup;
import model.Computer;
import model.Ports;
import model.Type;

public class StaxParser {

  public List<Computer> parseComputer(File xml) {
    List<Computer> computerList = new ArrayList<>();
    Computer computer = null;
    Type type = null;
    ComponentsGroup componentsGroup = null;
    Ports ports = null;
    List<Ports> portsList = null;

    XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
    try {
      XMLEventReader xmlEventReader = xmlInputFactory
          .createXMLEventReader(new FileInputStream(xml));
      while (xmlEventReader.hasNext()) {
        XMLEvent xmlEvent = xmlEventReader.nextEvent();
        if (xmlEvent.isStartElement()) {
          StartElement startElement = xmlEvent.asStartElement();
          String name = startElement.getName().getLocalPart();
          switch (name) {
            case "computer":
              computer = new Computer();
              Attribute attributes = startElement.getAttributeByName(new QName("computerNumber"));
              if (attributes != null) {
                computer.setComputerNumber(Integer.parseInt(attributes.getValue()));
              }
              break;
            case "computerName":
              xmlEvent = xmlEventReader.nextEvent();
              assert computer != null;
              computer.setComputerName(xmlEvent.asCharacters().getData());
              break;
            case "origin":
              xmlEvent = xmlEventReader.nextEvent();
              assert computer != null;
              computer.setOrigin(xmlEvent.asCharacters().getData());
              break;
            case "price":
              xmlEvent = xmlEventReader.nextEvent();
              assert computer != null;
              computer.setPrice(Integer.parseInt(xmlEvent.asCharacters().getData()));
              break;
            case "critical":
              xmlEvent = xmlEventReader.nextEvent();
              assert computer != null;
              computer.setCritical(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
              break;
            case "ports":
              xmlEvent = xmlEventReader.nextEvent();
              portsList = new ArrayList<>();
              break;
            case "port":
              xmlEvent = xmlEventReader.nextEvent();
              assert ports != null;
              portsList.add(new Ports(xmlEvent.asCharacters().getData()));
              break;
            case "types":
              xmlEvent = xmlEventReader.nextEvent();
              type = new Type();
              break;
            case "periphery":
              xmlEvent = xmlEventReader.nextEvent();
              assert type != null;
              type.setPeriphery(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
              break;
            case "energyConsumption":
              xmlEvent = xmlEventReader.nextEvent();
              assert type != null;
              type.setEnergyConsumption(Double.parseDouble(xmlEvent.asCharacters().getData()));
              break;
            case "cooler":
              xmlEvent = xmlEventReader.nextEvent();
              assert type != null;
              type.setCooler(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
              break;
            case "componentsGroup":
              xmlEvent = xmlEventReader.nextEvent();
              componentsGroup = new ComponentsGroup();
              break;
            case "processor":
              xmlEvent = xmlEventReader.nextEvent();
              componentsGroup.setProcessor(xmlEvent.asCharacters().getData());
              break;
            case "RAM":
              xmlEvent = xmlEventReader.nextEvent();
              assert componentsGroup != null;
              componentsGroup.setRAM(Integer.parseInt(xmlEvent.asCharacters().getData()));
              break;
            case "videoCard":
              xmlEvent = xmlEventReader.nextEvent();
              assert componentsGroup != null;
              componentsGroup.setVideoCard(xmlEvent.asCharacters().getData());
              break;
            case "motherBoard":
              xmlEvent = xmlEventReader.nextEvent();
              assert componentsGroup != null;
              componentsGroup.setMotherBoard(xmlEvent.asCharacters().getData());
              assert type != null;
              type.setComponentsGroup(componentsGroup);
              assert computer != null;
              computer.setType(type);
              computer.setPortsList(portsList);
              break;
          }
        }
        if (xmlEvent.isEndElement()) {
          EndElement endElement = xmlEvent.asEndElement();
          if (endElement.getName().getLocalPart().equals("computer")) {
            computerList.add(computer);
          }
        }
      }
    } catch (XMLStreamException | FileNotFoundException e) {
      e.printStackTrace();
    }
    return computerList;
  }
}
