package stax;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.xml.XMLConstants;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stax.StAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

public class StaxValidator {

  private String schemaName;
  private String xmlName;
  XMLStreamReader reader;
  SchemaFactory factory;
  Schema schema;
  Validator validator;
  private static Logger logger = LogManager.getLogger();

  public StaxValidator(String schemaName, String xmlName) {
    this.schemaName = schemaName;
    this.xmlName = xmlName;
  }

  public void validate() {
    try {
      reader = XMLInputFactory.newInstance()
          .createXMLStreamReader(new FileInputStream("computerXML.xml"));

      factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
      schema = factory.newSchema(new File("computerXSD.xsd"));

      validator = schema.newValidator();
      validator.validate(new StAXSource(reader));

      logger.debug("Document is valid");
    } catch (XMLStreamException | SAXException | IOException e) {
      e.printStackTrace();
    }
  }

}