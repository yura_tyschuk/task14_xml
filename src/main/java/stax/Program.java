package stax;

import java.io.File;
import java.util.List;
import model.Computer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Program {

  private static Logger logger = LogManager.getLogger();

  public static void main(String[] args) {
    String schemaName = "computerXSD.xsd";
    String xmlName = "computerXML.xml";
    StaxValidator staxValidator = new StaxValidator(schemaName, xmlName);
    staxValidator.validate();

    StaxParser staxParser = new StaxParser();
    List<Computer> computerList;
    File file = new File("computerXML.xml");
    computerList = staxParser.parseComputer(file);
    logger.warn(computerList);
  }

}
