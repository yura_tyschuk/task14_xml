package dom;

import java.io.File;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;

public class DomValidator {

  private String schemaName;
  private String xmlName;
  private Schema schema;
  private Document document;
  private static Logger logger = LogManager.getLogger();


  public DomValidator(String schemaName, String xmlName) {
    this.schemaName = schemaName;
    this.xmlName = xmlName;
    schema = loadSchema(this.schemaName);
    document = parseXmlDom(this.xmlName);
  }

  public void validateXml() {
    try {
      Validator validator = schema.newValidator();
      logger.debug("Validator Class: " + validator.getClass().getName());

      validator.validate(new DOMSource(document));
      logger.debug("Validation passed.");

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public Schema loadSchema(String schemaFileName) {
    Schema schema = null;
    try {
      String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
      SchemaFactory factory = SchemaFactory.newInstance(language);

      schema = factory.newSchema(new File(schemaFileName));
    } catch (Exception e) {
      e.printStackTrace();
    }
    return schema;
  }

  public Document parseXmlDom(String xmlName) {
    Document document = null;
    try {
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      document = builder.parse(new File(xmlName));
    } catch (Exception e) {
      e.printStackTrace();
    }
    return document;
  }
}
