package dom;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import model.Computer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class Program {

  private static Logger logger = LogManager.getLogger();

  public static void main(String[] args) {
    try {
      String schemaName = "computerXSD.xsd";
      String xmlName = "computerXML.xml";
      DomValidator domValidator = new DomValidator(schemaName, xmlName);
      domValidator.validateXml();

      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      Document document = builder.parse(new File("computerXML.xml"));
      List<Computer> computerList;
      DomParser domParser = new DomParser();
      computerList = domParser.readDoc(document);
      Collections.sort(computerList);
      logger.warn(computerList);
    } catch (ParserConfigurationException | SAXException | IOException e) {
      e.printStackTrace();
    }
  }

}
