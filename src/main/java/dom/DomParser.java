package dom;

import java.util.ArrayList;
import java.util.List;
import model.ComponentsGroup;
import model.Computer;
import model.Ports;
import model.Type;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DomParser {

  public List<Computer> readDoc(Document doc) {
    doc.getDocumentElement().normalize();
    List<Computer> computers = new ArrayList<>();
    NodeList nodeList = doc.getElementsByTagName("computer");

    for (int i = 0; i < nodeList.getLength(); i++) {
      Computer computer = new Computer();
      Type type;
      List<Ports> ports;
      ComponentsGroup componentsGroup;

      Node node = nodeList.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        Element element = (Element) node;

        computer.setComputerNumber(Integer.parseInt(element.getAttribute("computerNumber")));
        computer
            .setComputerName(element.getElementsByTagName("computerName").item(0).getTextContent());
        computer.setOrigin(element.getElementsByTagName("origin").item(0).getTextContent());
        computer.setPrice(
            Integer.parseInt(
                element.getElementsByTagName("price").item(0).getTextContent()));
        computer.setCritical(
            Boolean.parseBoolean(
                element.getElementsByTagName("critical").item(0).getTextContent()));

        ports = getPorts(element.getElementsByTagName("ports"));
        type = getType(element.getElementsByTagName("types"));
        componentsGroup = getComponentsGroup(element.getElementsByTagName("componentsGroup"));

        type.setComponentsGroup(componentsGroup);
        computer.setType(type);
        computer.setPortsList(ports);
        computers.add(computer);
      }
    }
    return computers;
  }

  private List<Ports> getPorts(NodeList nodes) {
    List<Ports> ports = new ArrayList<>();
    if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
      Element element = (Element) nodes.item(0);
      NodeList nodeList = element.getChildNodes();
      for (int i = 0; i < nodeList.getLength(); i++) {
        Node node = nodeList.item(i);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          Element el = (Element) node;
          ports.add(new Ports(el.getTextContent()));
        }
      }
    }

    return ports;
  }

  private Type getType(NodeList nodes) {
    Type type = new Type();
    if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
      Element element = (Element) nodes.item(0);
      type.setPeriphery(Boolean.parseBoolean(
          element.getElementsByTagName("periphery").item(0).getTextContent()));
      type.setEnergyConsumption(Double.parseDouble(
          element.getElementsByTagName("energyConsumption").item(0).getTextContent()));
      type.setCooler(
          Boolean.parseBoolean
              (element.getElementsByTagName("cooler").item(0).getTextContent()));
    }
    return type;
  }

  private ComponentsGroup getComponentsGroup(NodeList nodes) {
    ComponentsGroup componentsGroup = new ComponentsGroup();
    if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
      Element element = (Element) nodes.item(0);
      componentsGroup.setProcessor(
          element.getElementsByTagName("processor").item(0).getTextContent());
      componentsGroup.setRAM(
          Integer.parseInt(
              element.getElementsByTagName("RAM").item(0).getTextContent()));
      componentsGroup.setVideoCard(
          element.getElementsByTagName("videoCard").item(0).getTextContent());
      componentsGroup.setMotherBoard(
          element.getElementsByTagName("motherBoard").item(0).getTextContent());
    }
    return componentsGroup;
  }


}